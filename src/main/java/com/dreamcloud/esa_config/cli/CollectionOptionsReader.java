package com.dreamcloud.esa_config.cli;

import com.dreamcloud.esa_score.fs.*;
import com.dreamcloud.esa_score.score.*;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

public class CollectionOptionsReader extends OptionsReader {
    private static String TERM_INDEX_FILE = "collection.termIndexSource";
    private static String DOCUMENT_SCORE_FILE = "collection.documentScoreSource";
    private static String SCORE_MECHANISM = "collection.access";
    private static String SCORE_CACHE = "collection.cache";
    private static String LOG_ENABLED = "collection.log.enabled";
    private static String LOG_OUTPUT = "collection.log.output";
    private static String ID_TITLES_FILE = "collection.titleSource";

    private static TermIndex termIndex;

    public CollectionOptions getOptions(Properties properties) throws IOException {
        File termIndexSource = null;
        File documentScoreSource = null;

        if (hasProperty(properties, TERM_INDEX_FILE)) {
            termIndexSource = new File(properties.getProperty(TERM_INDEX_FILE));
        }

        if (hasProperty(properties, DOCUMENT_SCORE_FILE)) {
            documentScoreSource = new File(properties.getProperty(DOCUMENT_SCORE_FILE));
        }

        if (termIndexSource != null && documentScoreSource != null) {
            TermIndexReader termIndexReader = new TermIndexReader(termIndexSource);
            TermIndex termIndex = termIndexReader.readIndex();

            DocumentScoreDataReader scoreDataReader = null;
            if (hasProperty(properties, SCORE_MECHANISM)) {
                String storageProperty = properties.getProperty(SCORE_MECHANISM);
                if ("disk".equals(storageProperty)) {
                    scoreDataReader = new DocumentScoreFileReader(documentScoreSource);
                } else if("memory".equals(storageProperty)) {
                    scoreDataReader = new DocumentScoreMemoryReader(documentScoreSource);
                }
            } else {
                scoreDataReader = new DocumentScoreFileReader(documentScoreSource);
            }

            DocumentScoreReader scoreReader = new ScoreReader(termIndex, scoreDataReader);

            if (hasProperty(properties, SCORE_CACHE)) {
                String cacheProperty = properties.getProperty(SCORE_CACHE);
                if ("top-hits".equals(cacheProperty)) {
                    scoreReader = new DocumentScoreCachingReader(scoreReader);
                }
            }

            if (isEnabled(properties, LOG_ENABLED) && hasProperty(properties, LOG_OUTPUT)) {
                File logFile = new File(properties.getProperty(LOG_OUTPUT));
                scoreReader = new LoggingScoreReader(scoreReader, logFile);
            }

            DocumentNameResolver nameResolver = null;
            if (hasProperty(properties, ID_TITLES_FILE)) {
                nameResolver = new DocumentNameResolver(new File(properties.getProperty(ID_TITLES_FILE)));
            }

            return new CollectionOptions(termIndex, scoreReader, nameResolver);
        } else {
            return null;
        }
    }
}
