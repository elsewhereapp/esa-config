package com.dreamcloud.esa_config.cli;

import com.dreamcloud.esa_core.vectorizer.VectorizationOptions;

import java.util.Properties;

/**
 * A CLI utility class for reading the relevant command arguments for vectorization options.
 */
public class VectorizationOptionsReader extends OptionsReader {
    protected static String WINDOW_SIZE = "vector.windowSize";
    protected static String WINDOW_DROP = "vector.windowDrop";
    protected static String VECTOR_LIMIT = "vector.limit";
    protected static String CONCEPT_MULTIPLIER = "vector.conceptMultiplier";

    public VectorizationOptions getOptions(Properties properties) {
        VectorizationOptions options = new VectorizationOptions();

        if (hasProperty(properties, WINDOW_SIZE)) {
            options.setWindowSize(Integer.parseInt(properties.getProperty(WINDOW_SIZE)));
        }

        if (hasProperty(properties, WINDOW_DROP)) {
            options.setWindowDrop(Float.parseFloat(properties.getProperty(WINDOW_DROP)));
        }

        if (hasProperty(properties, VECTOR_LIMIT)) {
            options.setVectorLimit(Integer.parseInt(properties.getProperty(VECTOR_LIMIT)));
        }

        if (hasProperty(properties, CONCEPT_MULTIPLIER)) {
            options.setCommonConceptMultiplier(Float.parseFloat(properties.getProperty(CONCEPT_MULTIPLIER)));
        }

        options.display();

        return options;
    }
}
