package com.dreamcloud.esa_config.cli;

import com.dreamcloud.esa_core.analyzer.AnalyzerOptions;
import com.dreamcloud.esa_core.analyzer.FilterWordRepository;
import com.dreamcloud.esa_core.documentPreprocessor.*;

import java.io.IOException;
import java.util.*;

public class AnalyzerOptionsReader extends OptionsReader {
    protected static String MIN_WORD_LENGTH = "analyzer.term.minLength";
    protected static String MAX_WORD_LENGTH = "analyzer.term.maxLength";
    protected static String CLASSIC_ENABLED = "analyzer.classic.enabled";
    protected static String SINGULAR_ENABLED = "analyzer.singular.enabled";
    protected static String ASCII_ENABLED = "analyzer.ascii.enabled";
    protected static String LOWERCASE_ENABLED = "analyzer.lowercase.enabled";
    protected static String STEMMER_ENABLED = "analyzer.stemmer.enabled";
    protected static String STEMMER_DEPTH = "analyzer.stemmer.depth";

    public AnalyzerOptions getOptions(Properties properties) throws IOException {
        AnalyzerOptions options = new AnalyzerOptions();

        if (isEnabled(properties, STEMMER_ENABLED)) {
            options.setUsingPorterStemmer(true);
            if (properties.containsKey(STEMMER_DEPTH)) {
                String depth = properties.getProperty(STEMMER_DEPTH);
                options.setPorterStemmerDepth(Integer.parseInt(depth));
            }
        }

        if (isEnabled(properties, ASCII_ENABLED)) {
            options.setUsingAsciiFolding(true);
        }

        if (isEnabled(properties, CLASSIC_ENABLED)) {
            options.setUsingClassic(true);
        }

        if (isEnabled(properties, SINGULAR_ENABLED)) {
            options.setUsingSingularCase(true);
        }

        if (isEnabled(properties, LOWERCASE_ENABLED)) {
            options.setUsingLowerCase(true);
        }

        Collection<DocumentPreprocessor> preprocessorList = new ArrayList<>();
        for (String property: properties.stringPropertyNames()) {
            if (property.matches("^analyzer\\.preprocessor\\.[0-9]+$")) {
                String preprocessorName = properties.getProperty(property);
                switch (preprocessorName) {
                    case "wiki": preprocessorList.add(new WikiPreprocessor()); break;
                    case "standard": preprocessorList.add(new StandardPreprocessor()); break;
                    default: throw new IllegalArgumentException("Invalid document preprocessor '" + preprocessorName + "'");
                }
            }
        }
        if (preprocessorList.size() > 0) {
            options.setPreprocessor(new ChainedPreprocessor(preprocessorList));
        }

        if (hasProperty(properties, MIN_WORD_LENGTH)) {
            options.setMinimumWordLength(Integer.parseInt(properties.getProperty(MIN_WORD_LENGTH)));
        }

        if (hasProperty(properties, MAX_WORD_LENGTH)) {
            options.setMaximumWordLength(Integer.parseInt(properties.getProperty(MAX_WORD_LENGTH)));
        }

        //Loop to find everything about stop/go words
        properties.propertyNames();

        FilterWordRepository goWords = null;
        FilterWordRepository stopWords = null;

        for(String property: properties.stringPropertyNames()) {
            if (property.matches("^analyzer\\.(go|stop)\\.[0-9]+\\.source$")) {
                String source = properties.getProperty(property);
                if (!"".equals(source)) {
                    if (property.startsWith("analyzer.go")) {
                        if (goWords == null) {
                            goWords = new FilterWordRepository(options.isUsingLowerCase());
                        }
                        goWords.addSource(source);
                    } else {
                        if (stopWords == null) {
                            stopWords = new FilterWordRepository(options.isUsingLowerCase());
                        }
                        stopWords.addSource(source);
                    }
                }
            }
        }

        if (goWords != null) {
            options.setFilterWordRepository(goWords);
        }
        if (stopWords != null) {
            options.setStopWordsRepository(stopWords);
        }

        options.display();

        return options;
    }
}
