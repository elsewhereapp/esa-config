package com.dreamcloud.esa_config.cli;

import com.dreamcloud.esa_score.analysis.TfIdfOptions;

import java.util.Properties;

public class TfIdfOptionsReader extends OptionsReader {
    protected static String TFIDF_MODE = "scoring.mode";
    protected static String BM25_B = "scoring.bm25.b";
    protected static String BM25_K = "scoring.bm25.k";
    protected static String BM25_DELTA = "scoring.bm25.delta";


    public TfIdfOptions getOptions(Properties properties) {
        String tfIdfMode = "ltc";
        if (hasProperty(properties, TFIDF_MODE)) {
            tfIdfMode = properties.getProperty(TFIDF_MODE);
        }

        TfIdfOptions options = TfIdfOptions.fromString(tfIdfMode);

        if (hasProperty(properties, BM25_B)) {
            options.setBm25_b(Float.parseFloat(properties.getProperty(BM25_B)));
        }

        if (hasProperty(properties, BM25_K)) {
            options.setBm25_k(Float.parseFloat(properties.getProperty(BM25_K)));
        }

        if (hasProperty(properties, BM25_DELTA)) {
            options.setBm25_delta(Float.parseFloat(properties.getProperty(BM25_DELTA)));
        }

        options.display();

        return options;
    }
}
