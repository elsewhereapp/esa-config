package com.dreamcloud.esa_config.cli;

import java.util.Properties;

public class OptionsReader {
    public boolean isEnabled(Properties properties, String property) {
        if (properties.containsKey(property)) {
            String value = properties.getProperty(property).toLowerCase();
            return "1".equals(value) || "true".equals(value);
        } else {
            return false;
        }
    }

    public boolean hasProperty(Properties properties, String property) {
        return properties.containsKey(property) && !"".equals(properties.getProperty(property));
    }

}
